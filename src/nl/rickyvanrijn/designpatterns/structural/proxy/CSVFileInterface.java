package nl.rickyvanrijn.designpatterns.structural.proxy;

public interface CSVFileInterface {
	public String readFile(String filename);
	public void writeFile(String fileContent);
}
