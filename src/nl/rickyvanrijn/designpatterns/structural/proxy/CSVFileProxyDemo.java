package nl.rickyvanrijn.designpatterns.structural.proxy;

public class CSVFileProxyDemo {

	public static void main(String[] args) {
		CSVFileInterface csvFile = new CSVFileProxy();
		String content = csvFile.readFile("test.csv");
		content = "test;ricky;";
		csvFile.writeFile(content);
	}

}
