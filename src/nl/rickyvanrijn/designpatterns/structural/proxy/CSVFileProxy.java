package nl.rickyvanrijn.designpatterns.structural.proxy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public class CSVFileProxy implements CSVFileInterface {

	private String fName = "";
	
	@Override
	public String readFile(String filename) {
		fName = filename;
		File file = new File(fName);
		String fContent ="";
		
		if(!file.exists()){
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));

			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				fContent += sCurrentLine;
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return fContent;
	}

	@Override
	public void writeFile(String fileContent) {
		try{
			File file = new File(fName);

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(fileContent);
			bw.close();

		}catch(Exception e){
			e.printStackTrace();
		}

	}

}
