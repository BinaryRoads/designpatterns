package nl.rickyvanrijn.designpatterns.creational.factorymethod;

public interface DeveloperInterface {
	public String getOutputCode();
}
