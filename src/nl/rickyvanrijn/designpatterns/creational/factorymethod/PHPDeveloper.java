package nl.rickyvanrijn.designpatterns.creational.factorymethod;

public class PHPDeveloper implements DeveloperInterface {

	@Override
	public String getOutputCode() {
		return "echo(\"This outputs in PHP\");";
	}

}
