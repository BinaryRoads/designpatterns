package nl.rickyvanrijn.designpatterns.creational.factorymethod;

public class CDeveloper implements DeveloperInterface {

	@Override
	public String getOutputCode() {
		return "printf(\"This outputs in c format %d\", 1);";
	}

}
