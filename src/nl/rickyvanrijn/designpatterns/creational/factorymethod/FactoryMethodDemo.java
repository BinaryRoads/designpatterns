package nl.rickyvanrijn.designpatterns.creational.factorymethod;

import java.util.ArrayList;

public class FactoryMethodDemo {
	public static void main(String[] args) {
		ArrayList<DeveloperInterface> developers = new ArrayList<DeveloperInterface>();
		developers.add(new PHPDeveloper());
		developers.add(new JavaDeveloper());
		developers.add(new CDeveloper());
		
		for(DeveloperInterface developer : developers){
			System.out.println(developer.getOutputCode());
		}
	}
}
