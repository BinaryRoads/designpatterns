package nl.rickyvanrijn.designpatterns.creational.factorymethod;

public class JavaDeveloper implements DeveloperInterface {

	@Override
	public String getOutputCode() {
		return "System.out.println(\"This outputs on a java way.\");";
	}

}
