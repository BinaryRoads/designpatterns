package nl.rickyvanrijn.designpatterns.behavioral.observer;

public class ObserverDemo {

	public static void main(String[] args) {
		Project project = new Project();
		
		new JavaDeveloper(project);
		new TechnicalConsultant(project);
		
		project.setState(ProjectState.STARTING);
	}

}
