package nl.rickyvanrijn.designpatterns.behavioral.observer;

public enum ProjectState {
	STARTING, IN_PROGRESS, TEST,DONE
}
