package nl.rickyvanrijn.designpatterns.behavioral.observer;

public class TechnicalConsultant extends Observer {

	public TechnicalConsultant(Project newProject) {
		project = newProject;
		project.attach(this);
	}

	@Override
	public void writeRole() {
		String response = "";
		switch(project.getState()){
		case DONE:
			response = "Make sure the production server doesn't fail during the demo.";
			break;
		case IN_PROGRESS:
			response = "Make sure the servers perform well.";
			break;
		case STARTING:
			response = "Set up the OTAP environment.";
			break;
		case TEST:
			response = "Transfer the project from server to server";
			break;
		default:
			response = "Making friends with lots of managers.";
			break;
		}
		System.out.println(this.getClass().getSimpleName()+ ": "+response);

	}

}
