package nl.rickyvanrijn.designpatterns.behavioral.observer;

public class JavaDeveloper extends Observer {

	public JavaDeveloper(Project newProject) {
		project = newProject;
		project.attach(this);
	}

	@Override
	public void writeRole() {
		String response= "";
		switch(project.getState()){
		case STARTING:
			response = "Sets the requirements needed for the software.";
			break;
		case DONE:
			response = "Give a good demo for the audience.";
			break;
		case IN_PROGRESS:
			response = "Codes the implementation of the project.";
			break;
		case TEST:
			response = "Testing other developers code.";
			break;
		default:
			response = "Drink some good coffee.";
			break;
		}
		System.out.println(this.getClass().getSimpleName()+": "+response);
	}

}
