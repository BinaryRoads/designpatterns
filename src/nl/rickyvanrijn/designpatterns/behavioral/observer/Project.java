package nl.rickyvanrijn.designpatterns.behavioral.observer;

import java.util.ArrayList;

public class Project {
	private ArrayList<Observer> observers = new ArrayList<Observer>();
	
	private ProjectState projectState;
	public void attach( Observer observer ) {
		observers.add(observer);
	}

	public ProjectState getState() {
		return projectState;
	}
  
	public void setState( ProjectState newState){
		projectState = newState;
		notifyAllObservers();
	}

	private void notifyAllObservers() {
	  for (int i=0; i < observers.size(); i++) {
	    observers.get(i).writeRole();
	  }
	}
}
