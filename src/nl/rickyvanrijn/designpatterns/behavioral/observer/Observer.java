package nl.rickyvanrijn.designpatterns.behavioral.observer;

public abstract class Observer {
	protected Project project;
	public abstract void writeRole();
}
